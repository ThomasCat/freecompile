<!--

MIT License

Copyright (c) 2019 Owais Shaikh

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

-->

<!--ace library used here.-->

<html>
	<head>
    	<title>FreeCompile</title>
    	<meta content="">
    	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.2.8/ace.js"></script><!--Ace library-->
    	<link href="/blackboard.css" rel="stylesheet" type="text/css"> <!--Editor styles-->
		<meta name="viewport" content="width=device-width, initial-scale=1"> <!--Mobile viewport-->
		<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3pro.css"> <!--Mobile styles-->
		<link rel="icon" href="https://gitlab.com/5b43f91c61170e88de567951ebbec765/freecompile/raw/master/freecompilelogo.png">
    	<style>
			.outputBox{
    			border:1px solid red;
    			background-color: #121212;
    			color: #86fc62;
    			width: 99%;
    			height: 25%;
    			margin: 5px 0;
    			padding: 0.5%;
			}

			.restOfWebpage{
				position: relative;
				top: 0px;
			}

			.header{
				position: relative;
				left: 0px; 
				top: 0px; 
			}
			#codeEditor{
				width:99%;
    			height: 75%;
    			margin:5px 0;
    			padding: 0.5%;
				border:1px solid #a0a05d;
				background-color: #121212;
			}
		</style>
	</head>

	<body bgcolor="#0e0e16">
		<div width="10%" class="header">
    		
    		<button style="background-color: black; color: white; border-bottom-right-radius:8px;
    			border-bottom-left-radius:8px;border-top-left-radius:8px;
    			border-top-right-radius:8px; left: 2000px;" id="compile" onclick="saveData()"><b style="font-size:14px;">Compile and Run</b></button>&nbsp;&nbsp;
    		<script type="text/javascript">
    			function aboutDialog(){
    				alert("This project was created by Owais Shaikh for his Web Programming class at GIT Belgaum.\n--------\nFreeCompile is a fast, simple, free and open-source all-in-one editor and builder.\nIt supports syntax highlighting, debugging, etc.\n\n");
    			}
    		</script>
    		<select id="language" name="Languages" style="background-color: black; color: white; background-color: black; color: white; border-bottom-right-radius:8px;
    			border-bottom-left-radius:8px;border-top-left-radius:8px;
    			border-top-right-radius:8px;">
				<option value="None" onclick="setNone()">--</option>
				<option value="C" onclick="setC()">C</option>
				<option value="Cpp" onclick="setCpp()">C++</option>
				<option value="Java" onclick="setJava()">Java</option>
				<option value="Python" onclick="setPython()">Python</option>
				<option value="PHP" onclick="setPHP()">PHP</option>
				<option value="Bash" onclick="setBash()">Bash</option>
				<option value="Assembly" onclick="setAssembly()">Assembly</option>
				<option value="Objective-C" onclick="setObjC()">Objective-C</option>
				<option value="Perl" onclick="setPerl()">Perl</option>
				<option value="Rust" onclick="setRust()">Rust</option>
			</select>
			<script type="text/javascript">
				function setNone(){
					var isNone=1;
					alert("No language selected, please make a selection.");
    				var langCookie=document.cookie='lang=None'; 
    				return isNone;
    			}

    			function setC(){
					var isNone=0;
    				var langCookie=document.cookie='lang=C'; 
    				return isNone;
    			}

    			function setCpp(){
					var isNone=0;
    				var langCookie=document.cookie='lang=Cpp';
    			}

    			function setJava(){
					var isNone=0;
    				var langCookie=document.cookie='lang=Java';
    			}

    			function setPython(){
					var isNone=0;
    				var langCookie=document.cookie='lang=Python';
    			}

    			function setPHP(){
					var isNone=0;
    				var langCookie=document.cookie='lang=PHP';
    			}

    			function setBash(){
					var isNone=0;
    				var langCookie=document.cookie='lang=Bash';
    			}

    			function setAssembly(){
					var isNone=0;
    				var langCookie=document.cookie='lang=Assembly';
    			}

    			function setObjC(){
					var isNone=0;
    				var langCookie=document.cookie='lang=ObjC';
    			}

    			function setPerl(){
					var isNone=0;
    				var langCookie=document.cookie='lang=Perl';
    			}

    			function setRust(){
					var isNone=0;
    				var langCookie=document.cookie='lang=Rust';
    			}

    		</script>
			<label style="position: absolute; right: 10px; top: 0px; color: white;" onclick="aboutDialog()">About</label>
			
		</div>
	
		<center>
			<div id="codeEditor"><?php $codeRead=$_COOKIE['code']; $lineBreakParse=str_replace(" ¥br¥ ","\n","$codeRead"); $ltParse=str_replace("<","&lt;","$lineBreakParse"); $gtParse=str_replace(">","&gt;","$ltParse"); /*We parse lt and gt because PHP ignores them*/$codeVar=$gtParse ;echo $codeVar; ?></div>
			<script type="text/javascript">
				var editor=ace.edit("codeEditor");
				editor.setTheme("ace/theme/terminal");
				editor.session.setNewLineMode("unix"); 
				editor.session.setMode("ace/mode/c_cpp");
				
				function saveData(){
					var enteredCode=editor.getValue();
					//Yen symbol is not commonly used, so I'm using it instead of other symbols
					var parsedCode=enteredCode.replace(new RegExp("\n","g")," ¥br¥ ");  //not parsing into single line doesnt save code into cookie properly.
					var codeCookie=document.cookie='code='+encodeURIComponent(parsedCode); 
					location.reload();
				}
			</script>
			<?php
				$lang=$_COOKIE['lang'];
				$codeRead=$_COOKIE['code'];
				$codeVar=str_replace(" ¥br¥ ","\n","$codeRead");  //since line breaks aren't recognized, replace </br/> with newline \n
				if ($lang=="C") {
    				$myFile=fopen("cprogram.c", "w") or die("Unable to open file.");
					fwrite($myFile, $codeVar);
					fclose($myFile);
				}else if($lang=="Cpp"){
					$myFile=fopen("cppprogram.cpp", "w") or die("Unable to open file.");
					fwrite($myFile, $codeVar);
					fclose($myFile);
				}else if($lang=="Java"){
					$myFile=fopen("javaprogram.java", "w") or die("Unable to open file.");
					fwrite($myFile, $codeVar);
					fclose($myFile);
				}else if($lang=="Python"){
					$myFile=fopen("pythonscript.py", "w") or die("Unable to open file.");
					fwrite($myFile, $codeVar);
					fclose($myFile);
				}else if($lang=="PHP"){
					$myFile=fopen("phpscript.php", "w") or die("Unable to open file.");
					fwrite($myFile, $codeVar);
					fclose($myFile);
				}else if($lang=="Bash"){
					$myFile=fopen("bashscript.sh", "w") or die("Unable to open file.");
					fwrite($myFile, $codeVar);
					fclose($myFile);
				}else if($lang=="Assembly"){
					$myFile=fopen("assemblyprogram.asm", "w") or die("Unable to open file.");
					fwrite($myFile, $codeVar);
					fclose($myFile);
				}else if($lang=="ObjC"){
					$myFile=fopen("objcprogram.cpp", "w") or die("Unable to open file.");
					fwrite($myFile, $codeVar);
					fclose($myFile);
				}else if($lang=="Perl"){
					$myFile=fopen("perlprogram.pl", "w") or die("Unable to open file.");
					fwrite($myFile, $codeVar);
					fclose($myFile);
				}else if($lang=="Rust"){
					$myFile=fopen("rustprogram.rs", "w") or die("Unable to open file.");
					fwrite($myFile, $codeVar);
					fclose($myFile);
				}
			?>
			<!--Make sure to run sudo chown -R http:http /srv/http && sudo chmod -R 777 /srv/http-->
			
		</center>
   		<div id=consoleOutput onclick="reloadConsole()">
			<center><textarea class="outputBox"><?php
					$lang=$_COOKIE['lang'];
					if ($lang=="C") {
    					echo exec("gcc cprogram.c -o cprogram && ./cprogram");
					}else if($lang=="Cpp"){
						echo exec("g++ -o cppprogram cppprogram.cpp && ./cppprogram");
					}else if($lang=="Java"){
						echo exec("javac javaprogram.java && java javaprogram");
					}else if($lang=="Python"){
						echo exec("chmod u+x pythonscript.py && python3 pythonscript.py");
					}else if($lang=="PHP"){
						echo exec("php phpscript.php");
					}else if($lang=="Bash"){
						echo exec("chmod u+x bashscript.sh && ./bashscript");
					}else if($lang=="Assembly"){
						echo exec("gcc cprogram.c -o cprogram && ./cprogram");
					}else if($lang=="ObjC"){
						echo exec("gcc cprogram.c -o cprogram && ./cprogram");
					}else if($lang=="Perl"){
						echo exec("gcc cprogram.c -o cprogram && ./cprogram");
					}else if($lang=="Rust"){
						echo exec("gcc cprogram.c -o cprogram && ./cprogram");
					}?>	
				</textarea></center>
		</div>

    	<p align="center" style="position: relative; top: -10px;color: cyan; font-size: 12px; "><a href="https://is.gd/_owais"><font color="cyan">Created by Owais Shaikh</font></a><font color="white">&nbsp;&nbsp;|&nbsp;&nbsp;</font><a href="https://gitlab.com/5b43f91c61170e88de567951ebbec765/freecompile"><font color="lime">Source code</font></a></p>
	</body>
</html>