# FreeCompile

An online all-in-one text editor with build support. Made for my Web Programming Assignment at [GIT, Belgaum](https://git.edu)

### Why did I make this?
It's inefficient to install, configure and use giant IDEs like CodeBlocks, NetBeans etc in schools and colleges for small, practice programs, like writing for loops and printing simple things.

### Why is this special?
Because it is completely free and open-source. You can visit the url given above. Or if you're a school or university, you can deploy this system on a centralized server. Even the server-side code is published here.

Also, it's extremely easy to use. Just select your preferred language from the dropdown and begin typing. Once it's done, just click 'Compile'. Any errors show up on the left hand side gutter, and compiler output is visible at the bottom.

## How to use (on local machine/server):
1. Change to your httpd root directory (eg. ```cd /srv/http```) and give it full read write execute permissions (ie. ```chmod 777```)
2. Clone this project by typing ```git clone https://gitlab.com/ThomasCat/freecompile```
3. Change to the FreeCompile directory and give it and its subfolders full read write execute permissions (ie. ```chmod 777```).
4. Open your browser and go to the http server address (eg. ```localhost/freecompile/public```)


### Improvements to be made

Add more language support, allow user input in console, allow drag and drop of file into editor area.


<hr>

[Open-source notices and Credits](NOTICE)

<b>License</b>:<br>
<a href="https://opensource.org/licenses/MIT" rel="nofollow"><img src="http://pre13.deviantart.net/4938/th/pre/f/2016/070/3/b/mit_license_logo_by_excaliburzero-d9ur2lg.png" alt="MITL Image" data-canonical-src="https://opensource.org/files/OSIApproved_1.png" width="60"></a><br>[Copyright © Owais Shaikh 2019](LICENSE)
<br><a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/80x15.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
